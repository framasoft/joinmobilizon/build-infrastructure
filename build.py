import os
from pathlib import Path

osList = {
    'debian': ["buster", "bullseye", "bookworm"],
    'ubuntu': ["focal", "jammy"],
    'fedora': ['38', '39'],
    'alpine': ["3.18.6", "3.19.1"]
    }

erlangVersion = "26.1.2"
elixirVersion = "1.15.7"

path = Path().resolve()

print(f'Path is {path}')

# for osName,versions in osList.items():
#     for version in versions:
#         os.system(f'{os.path.join(path, "erlang.sh")} {erlangVersion} {osName} {version} amd64')

# for osName,versions in osList.items():
#     for version in versions:
#         os.system(f'{os.path.join(path, "elixir.sh")} {elixirVersion} {erlangVersion} {osName} {version} amd64')

for osName,versions in osList.items():
    for version in versions:
        os.system(f'{os.path.join(path, "build.sh")} {osName} {version} amd64')