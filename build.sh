
#!/bin/bash

set -euox pipefail

elixir="1.16.1"
erlang="26.2.2"

os=$1
os_version=$2
arch=$3

if [ "${os}" == "fedora" ]; then
    ./erlang.sh ${erlang} ${os} ${os_version} ${arch}
    ./elixir.sh ${elixir} ${erlang} ${os} ${os_version} ${arch}
fi

tag=${elixir}-erlang-${erlang}-${os}-${os_version}
finaltag=${tag}
buildpack="buildpack-${os}"
if [ "${os}" == "ubuntu" ]; then
    buildpack="buildpack-debian"
    if [ "${os_version}" == "focal" ]; then
        tag="${tag}-20240123"
    else
        tag="${tag}-20240125"
    fi
fi
if [ "${os}" == "debian" ]; then
    tag="${tag}-20240130"
fi

docker build --pull -t mobilizon/buildpack:${finaltag} --build-arg BASE_TAG=${tag} -f ${buildpack}.dockerfile .

# Smoke test
docker run --rm mobilizon/buildpack:${finaltag} elixir -v

docker push mobilizon/buildpack:${finaltag}