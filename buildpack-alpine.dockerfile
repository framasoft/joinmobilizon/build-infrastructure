ARG BASE_TAG

FROM docker.io/hexpm/elixir:${BASE_TAG}

ENV TZ Etc/UTC

RUN apk add --no-cache --update \
  gcc \
  make \
  git \
  curl \
  cmake \
  libc-dev \
  g++

RUN mix local.hex --force && mix local.rebar --force