ARG BASE_TAG

FROM docker.io/hexpm/elixir:${BASE_TAG}

ENV DEBIAN_FRONTEND noninteractive
ENV TZ Etc/UTC

RUN apt-get update && apt-get install -yq build-essential git curl cmake

RUN mix local.hex --force && mix local.rebar --force