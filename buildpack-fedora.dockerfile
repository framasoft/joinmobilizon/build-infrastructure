ARG BASE_TAG

FROM mobilizon/elixir:${BASE_TAG}

RUN echo "install_weak_deps=false" >> /etc/dnf/dnf.conf && dnf -y update && dnf -y install make automake gcc gcc-c++ git curl cmake && dnf clean all

RUN mix local.hex --force && mix local.rebar --force
