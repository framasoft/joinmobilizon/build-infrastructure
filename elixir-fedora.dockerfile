ARG ARCH
ARG OS_VERSION
ARG ERLANG

FROM fedora:${OS_VERSION} AS build

RUN echo "install_weak_deps=false" >> /etc/dnf/dnf.conf && \
  dnf -y upgrade && \
  dnf -y install \
  wget \
  ca-certificates \
  unzip \
  make \
  && dnf clean all

ARG ELIXIR
ARG ERLANG_MAJOR

RUN wget -q -O elixir.zip "https://repo.hex.pm/builds/elixir/v${ELIXIR}-otp-${ERLANG_MAJOR}.zip" && unzip -d /ELIXIR elixir.zip
WORKDIR /ELIXIR
RUN make -o compile DESTDIR=/ELIXIR_LOCAL install

FROM mobilizon/erlang:${ERLANG}-fedora-${OS_VERSION} AS final

COPY --from=build /ELIXIR_LOCAL/usr/local /usr/local