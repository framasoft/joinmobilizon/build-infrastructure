#!/bin/bash

set -euox pipefail

erlang=$1
os=$2
os_version=$3
arch=$4

tag=${erlang}-${os}-${os_version}

case "${os}" in
  "fedora")
    dockerfile="erlang-fedora.dockerfile"
    ;;
esac


# Disable PIE for OTP prior to 21; see http://erlang.org/doc/apps/hipe/notes.html#hipe-3.18
pie_cflags="-fpie"
pie_ldflags="-pie"
if [ "$(echo ${erlang} | cut -d '.' -f 1)" -le "20" ]; then
  pie_cflags=""
  pie_ldflags=""
fi

# Disable -fcf-protection on non x86 architectures
cf_protection="-fcf-protection=full"
if [ "${arch}" != "amd64" ]; then
  cf_protection=""
fi


docker build \
  -t mobilizon/erlang:${tag} \
  --build-arg ERLANG=${erlang} \
  --build-arg OS_VERSION=${os_version} \
  --build-arg PIE_CFLAGS=${pie_cflags} \
  --build-arg PIE_LDFLAGS=${pie_ldflags} \
  --build-arg CF_PROTECTION=${cf_protection} \
  -f ${dockerfile} .

# Smoke test
docker run --rm mobilizon/erlang:${tag} erl -eval '{ok, Version} = file:read_file(filename:join([code:root_dir(), "releases", erlang:system_info(otp_release), "OTP_VERSION"])), io:fwrite(Version), halt().' -noshell

# This command have a tendancy to intermittently fail
docker push mobilizon/erlang:${tag}